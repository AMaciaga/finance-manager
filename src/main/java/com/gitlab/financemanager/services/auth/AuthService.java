package com.gitlab.financemanager.services.auth;

/**
 * A service that manages authorization of users in the system.
 *
 * @author Konrad Kleczkowski
 */
public interface AuthService {
    /**
     * Registers an user into the database.
     *
     * @param phoneNumber a phone number that identifies a new user
     * @param nickname    a nick name for new user
     * @throws UserAlreadyExistsException when user were registered before
     */
    void register(String phoneNumber, String nickname) throws UserAlreadyExistsException;

    /**
     * Challenges an existing user for a new one-time password.
     *
     * @param phoneNumber the phone number of the user
     * @throws NoSuchUserToChallengeException when user does not exist
     */
    void challenge(String phoneNumber) throws NoSuchUserToChallengeException;

    /**
     * Edits details of existing wallet.
     *
     * @param id          the ID of existing user
     * @param newName     the new name of the user, <code>null</code> if not specified
     * @throws NoSuchUserToChallengeException    when user with given username does not exist
     * @throws IllegalArgumentException if <code>newName</code> and <code>newCurrency</code> are both <code>null</code>
     */
    void editUser(Long id, String newName) throws NoSuchUserToChallengeException;
}
