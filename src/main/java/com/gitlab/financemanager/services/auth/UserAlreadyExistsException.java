package com.gitlab.financemanager.services.auth;

/**
 * Thrown when an user already exists in the database.
 *
 * @author Konrad Kleczkowski
 */
public class UserAlreadyExistsException extends Exception {
    public UserAlreadyExistsException(String phoneNumber) {
        super(String.format("User %s does not exists", phoneNumber));
    }
}
