package com.gitlab.financemanager.services.auth.sms;

/**
 * A service that sends SMS to certain phone number.
 *
 * @author Konrad Kleczkowski
 */
public interface SmsSender {
    /**
     * Send SMS to given phone number.
     *
     * @param phoneNumber the phone number
     * @param message     the message
     * @throws SmsSenderException when a SMS service fails
     */
    void sendSms(String phoneNumber, String message);
}
