package com.gitlab.financemanager.services.auth.jwt;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.auth.SimpleGrantedAuthority;
import com.gitlab.financemanager.services.repos.UserRepository;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An intercepting filter that validates JWT token for obtaining user authorities.
 *
 * @author Konrad Kleczkowski
 */
@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    private String secretKey;
    private UserRepository userRepository;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  String secretKey) {
        super(authenticationManager);
        this.secretKey = secretKey;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        Optional<UsernamePasswordAuthenticationToken> auth = getAuthentication(request);
        if (auth.isEmpty()) {
            chain.doFilter(request, response);
            return;
        }
        SecurityContextHolder.getContext().setAuthentication(auth.get());
        chain.doFilter(request, response);
    }

    private Optional<UsernamePasswordAuthenticationToken> getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(JwtConstants.TOKEN_HEADER);

        if (StringUtils.isNotEmpty(token) && token.startsWith(JwtConstants.TOKEN_PREFIX)) {
            try {
                byte[] signingKey = secretKey.getBytes();

                Jws<Claims> parsedToken = Jwts.parser()
                        .setSigningKey(signingKey)
                        .parseClaimsJws(token.replace(JwtConstants.TOKEN_PREFIX, ""));

                String username = parsedToken.getBody().getSubject();

                List<String> rawRoles = ((List<?>) parsedToken.getBody().get("rol"))
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.toList());
                List<GrantedAuthority> authorities = rawRoles
                        .stream()
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

                if (username != null && !username.isEmpty()) {
                    Authentication auth = getAuthenticationManager()
                            .authenticate(new UsernamePasswordAuthenticationToken(username, null));
                    return Optional.ofNullable(((UsernamePasswordAuthenticationToken) auth));
//                    return userRepository
//                            .findUserByPhoneNumber(username)
//                            .map(User::getUserDetails)
//                            .filter(ud -> rawRoles.equals(ud
//                                    .getAuthorities()
//                                    .stream()
//                                    .map(GrantedAuthority::getAuthority)
//                                    .collect(Collectors.toList())))
//                            .map(ud -> new UsernamePasswordAuthenticationToken(ud, null, ud.getAuthorities()));
                }
            } catch (ExpiredJwtException e) {
                log.warn("JWT authorization failed. Someone attempted to validate expired token `{}`", token, e);
            } catch (UnsupportedJwtException e) {
                log.warn("JWT authorization failed. Someone attempted to parse unsupported JWT token `{}`", token, e);
            } catch (MalformedJwtException e) {
                log.warn("JWT authorization failed. Someone attempted to parse malformed JWT token `{}`", token, e);
            } catch (SignatureException e) {
                log.warn("JWT authorization failed. Someone attempted to parse JWT token `{}` with invalid signature", token, e);
            } catch (IllegalArgumentException e) {
                log.warn("JWT authorization failed. Someone attempted to parse null or empty JWT token `{}`", token, e);
            }
        }

        return Optional.empty();
    }
}
