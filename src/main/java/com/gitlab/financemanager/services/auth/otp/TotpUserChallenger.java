package com.gitlab.financemanager.services.auth.otp;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.services.auth.sms.SmsSender;
import com.gitlab.financemanager.services.auth.sms.SmsSenderException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.time.Instant;

/**
 * A time-based one-time password user challenger.
 * <p>
 * Service needs specified secret key
 * using <code>security.otp.secret</code> variable.
 *
 * @author Konrad Kleczkowski
 */
//@Service
@Slf4j
public class TotpUserChallenger implements UserChallenger {
    private final TimeBasedOneTimePasswordGenerator passwordGenerator;
    private final SmsSender smsSender;
    @Value("${security.otp.secret}")
    private String otpSecret;

    @Autowired
    public TotpUserChallenger(TimeBasedOneTimePasswordGenerator passwordGenerator, SmsSender smsSender) {
        this.passwordGenerator = passwordGenerator;
        this.smsSender = smsSender;
    }

    @Override
    public void challenge(User user) {
        try {
            String otp = Integer.toHexString(passwordGenerator.generateOneTimePassword(getKey(user), Instant.now()));
            smsSender.sendSms(user.getPhoneNumber(), otp);
        } catch (InvalidKeyException e) {
            throw new ChallengeException("Cannot challenge because key is malformed", e);
        } catch (SmsSenderException e) {
            throw new ChallengeException("Cannot send challenge via SMS sender", e);
        }
    }

    @Override
    public boolean verify(User user, String payload) {
        try {
            String expected = Integer.toHexString(passwordGenerator.generateOneTimePassword(getKey(user), Instant.now()));
            return expected.equals(payload);
        } catch (InvalidKeyException e) {
            log.warn("Cannot verify because key is malformed, refusing to verify", e);
            return false;
        }
    }

    private Key getKey(User user) {
        String userSecret = String.format("%s:%s:%s",
                user.getPhoneNumber(),
                otpSecret,
                user.getUserDetails().getPassword());
        return new SecretKeySpec(userSecret.getBytes(), passwordGenerator.getAlgorithm());
    }
}
