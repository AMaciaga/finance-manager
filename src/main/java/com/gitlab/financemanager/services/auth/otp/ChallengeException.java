package com.gitlab.financemanager.services.auth.otp;

/**
 * Thrown when an error has been occurred during user challenging.
 *
 * @author Konrad Kleczkowski
 */
public class ChallengeException extends RuntimeException {
    public ChallengeException(String message) {
        super(message);
    }

    public ChallengeException(String message, Exception cause) {
        super(message, cause);
    }
}
