package com.gitlab.financemanager.services.auth;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.auth.SimpleGrantedAuthority;
import com.gitlab.financemanager.entities.auth.SimpleUserDetails;
import com.gitlab.financemanager.services.auth.otp.UserChallenger;
import com.gitlab.financemanager.services.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * An implementation of {@link AuthService}.
 *
 * @author Konrad Kleczkowski
 */
@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final UserChallenger userChallenger;

    @Autowired
    public AuthServiceImpl(UserRepository userRepository, UserChallenger userChallenger) {
        this.userRepository = userRepository;
        this.userChallenger = userChallenger;
    }

    @Override
    public void register(String phoneNumber, String nickname) throws UserAlreadyExistsException {
        if (userRepository.findUserByPhoneNumber(phoneNumber).isPresent()) {
            throw new UserAlreadyExistsException(phoneNumber);
        }

        User user = User.builder()
                .phoneNumber(phoneNumber)
                .nickName(nickname)
                .build();

        SimpleUserDetails details = SimpleUserDetails.builder()
                .user(user)
                .password(UUID.randomUUID().toString())
                .authorities(List.of(new SimpleGrantedAuthority("USER")))
                .build();

        user.setUserDetails(details);

        userChallenger.challenge(user);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void challenge(String phoneNumber) throws NoSuchUserToChallengeException {
        Optional<User> user = userRepository.findUserByPhoneNumber(phoneNumber);
        if (user.isEmpty()) {
            throw new NoSuchUserToChallengeException(phoneNumber);
        }
        userChallenger.challenge(user.get());
    }

    @Override
    public void editUser(Long id, String newName) throws NoSuchUserToChallengeException {
        if(newName == null) throw new IllegalArgumentException();

            User newUser = userRepository
                    .findById(id)
                    .orElseThrow(() -> new NoSuchUserToChallengeException(newName))
                    .toBuilder()
                    .nickName(newName)
                    .build();
            userRepository.saveAndFlush(newUser);
    }
}
