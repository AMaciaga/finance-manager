package com.gitlab.financemanager.services.auth.otp;

import com.gitlab.financemanager.entities.User;

/**
 * A service that is able to challenge user using one-time password.
 *
 * @author Konrad Kleczkowski
 */
public interface UserChallenger {
    /**
     * Send challenge to user and wait for his proof.
     *
     * @param user an user
     * @throws ChallengeException when an error occurs during challenge
     */
    void challenge(User user) throws ChallengeException;

    /**
     * Verify user's payload to give further access to the system.
     *
     * @param user    an user
     * @param payload a sent payload by user
     * @return <code>true</code> if user has been verified successfully
     */
    boolean verify(User user, String payload);
}
