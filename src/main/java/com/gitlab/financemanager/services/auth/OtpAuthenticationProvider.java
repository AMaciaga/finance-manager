package com.gitlab.financemanager.services.auth;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.auth.SimpleUserDetails;
import com.gitlab.financemanager.services.auth.otp.UserChallenger;
import com.gitlab.financemanager.services.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * An authentication provider that checks OTP that were challenged.
 *
 * @author Konrad Kleczkowski
 */
@Service
public class OtpAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private final UserRepository userRepository;
    private final UserChallenger userChallenger;

    @Autowired
    public OtpAuthenticationProvider(UserRepository userRepository, UserChallenger userChallenger) {
        this.userRepository = userRepository;
        this.userChallenger = userChallenger;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication) throws BadCredentialsException {
        String otp = (String) authentication.getCredentials();
        if (!userChallenger.verify(((SimpleUserDetails) userDetails).getUser(), otp)) {
            throw new BadCredentialsException("OTP does not match");
        }
    }

    @Override
    protected UserDetails retrieveUser(String username,
                                       UsernamePasswordAuthenticationToken authentication) throws UsernameNotFoundException {
        Optional<User> maybeUser = userRepository.findUserByPhoneNumber(username);
        if (maybeUser.isEmpty()) throw new UsernameNotFoundException("User " + username + " cannot be found");
        return maybeUser.get().getUserDetails();
    }
}
