package com.gitlab.financemanager.services.auth.sms;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * A SMS sender that uses Textbelt.
 *
 * @author Konrad Kleczkowski
 */
@Service
@Slf4j
public class TextbeltSmsSender implements SmsSender {
    private final static String TEXTBELT_API_URL = "https://textbelt.com/text";

    @Value("${sms.textbelt.key:textbelt}")
    private String textbeltKey;

    @Override
    public void sendSms(String phoneNumber, String message) {
        NameValuePair[] requestData = {
                new BasicNameValuePair("phone", phoneNumber),
                new BasicNameValuePair("message", message),
                new BasicNameValuePair("key", textbeltKey)
        };

        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(TEXTBELT_API_URL);
        try {
            request.setEntity(new UrlEncodedFormEntity(Arrays.asList(requestData)));
            HttpResponse response = client.execute(request);
            JsonParser jsonParser = JsonParserFactory.getJsonParser();
            Map<String, Object> responseMap =
                    jsonParser.parseMap(EntityUtils.toString(response.getEntity()));
            Boolean success = (Boolean) responseMap.get("success");
            if (!success) {
                String error = (String) responseMap.get("error");
                throw new SmsSenderException(error);
            } else {
                log.info("SMS has been sent to {}", phoneNumber);
            }
        } catch (IOException e) {
            log.error("During SMS sending I/O error has been occurred", e);
            throw new SmsSenderException(e);
        }
    }
}
