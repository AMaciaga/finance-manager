package com.gitlab.financemanager.services.auth.sms;

/**
 * Thrown when SMS sender has failed.
 *
 * @author Konrad Kleczkowski
 */
public class SmsSenderException extends RuntimeException {
    public SmsSenderException(String message) {
        super(message);
    }

    public SmsSenderException(Throwable e) {
        super(e);
    }
}
