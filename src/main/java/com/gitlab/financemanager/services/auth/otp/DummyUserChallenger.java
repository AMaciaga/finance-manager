package com.gitlab.financemanager.services.auth.otp;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import com.gitlab.financemanager.entities.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class DummyUserChallenger implements UserChallenger {
    private final TimeBasedOneTimePasswordGenerator passwordGenerator;
    private final Map<String, Integer> otps = new HashMap<>();
    private final Map<String, LocalDateTime> dueTime = new HashMap<>();

    @Value("${security.otp.secret}")
    private String otpSecret;

    @Autowired
    public DummyUserChallenger(TimeBasedOneTimePasswordGenerator passwordGenerator) {
        this.passwordGenerator = passwordGenerator;
    }

    @Override
    public void challenge(User user) throws ChallengeException {
        try {
            int password = passwordGenerator.generateOneTimePassword(getKey(user), Instant.now());
            log.info("One-time password for {} is {}", user.getPhoneNumber(), password);
            otps.put(user.getPhoneNumber(), password);
        } catch (InvalidKeyException e) {
            throw new ChallengeException("Cannot challenge because key is malformed", e);
        }
    }

    @Override
    public boolean verify(User user, String payload) {
        if (dueTime.containsKey(user.getPhoneNumber())
                && dueTime.get(user.getPhoneNumber()).isAfter(LocalDateTime.now())) {
            log.info("User {} ({}) verified automatically because verification due time does not past",
                    user.getNickName(), user.getPhoneNumber());
            return true;
        }
        if (!otps.containsKey(user.getPhoneNumber())) {
            log.info("Verification failed: challenge was not requested by user {} ({})",
                    user.getNickName(), user.getPhoneNumber());
            return false;
        }
        try {
            if (otps.get(user.getPhoneNumber()) != Integer.parseInt(payload)) {
                log.info("Verification failed: user {} ({}) sent password that does not match, given: {} expected: {}",
                        user.getNickName(), user.getPhoneNumber(),
                        payload, otps.get(user.getPhoneNumber()));
                return false;
            }
        } catch (NumberFormatException e) {
            log.info("Verification failed: user {} ({}) sent password that is not a number",
                    user.getNickName(), user.getPhoneNumber());
            return false;
        }
        otps.remove(user.getPhoneNumber());
        dueTime.put(user.getPhoneNumber(), LocalDateTime.now().plusDays(1));
        log.info("User {} ({}) verified the challenge successfully", user.getNickName(), user.getPhoneNumber());
        return true;
    }

    private Key getKey(User user) {
        String userSecret = String.format("%s:%s:%s",
                user.getPhoneNumber(),
                otpSecret,
                user.getUserDetails().getPassword());
        return new SecretKeySpec(userSecret.getBytes(), passwordGenerator.getAlgorithm());
    }
}
