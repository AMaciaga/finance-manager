package com.gitlab.financemanager.services.auth;

/**
 * Thrown when user does not exist during challenge procedure.
 *
 * @author Konrad Kleczkowski
 */
public class NoSuchUserToChallengeException extends Exception {
    /**
     * Creates an exception with an user name.
     *
     * @param username the user name
     */
    public NoSuchUserToChallengeException(String username) {
        super(String.format("User %s does not exist, challenge aborted", username));
    }
}
