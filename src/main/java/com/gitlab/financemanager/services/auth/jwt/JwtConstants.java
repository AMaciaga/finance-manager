package com.gitlab.financemanager.services.auth.jwt;

/**
 * Constants for JWT support.
 *
 * @author Konrad Kleczkowski
 */
public final class JwtConstants {
    static final String AUTH_LOGIN_URL = "/auth/login";
    static final String TOKEN_HEADER = "Authorization";
    static final String TOKEN_PREFIX = "Bearer ";
    static final String TOKEN_TYPE = "JWT";
    static final String TOKEN_ISSUER = "financemanager-api";
    static final String TOKEN_AUDIENCE = "financemanager-app";

    public JwtConstants() {
        throw new IllegalStateException("You don't really need to instantiate this class. Trust me.");
    }
}
