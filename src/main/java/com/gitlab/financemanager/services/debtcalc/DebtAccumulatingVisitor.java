package com.gitlab.financemanager.services.debtcalc;

import com.gitlab.financemanager.entities.expense.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * An implementation of {@link ExpenseVisitor} that accumulates
 * debts of a debtor that were issued by an issuer.
 *
 * @author Konrad Kleczkowski
 */
public class DebtAccumulatingVisitor implements ExpenseVisitor {
    private BigDecimal result = BigDecimal.ZERO;
    private BigDecimal percentTotal;

    /**
     * Returns an accumulated debt.
     *
     * @return a big decimal that describes accumulated debt
     */
    public BigDecimal getResult() {
        return result;
    }

    @Override
    public void visitCustomPart(CustomExpensePart part) {
        result = result.add(part.getAmount());
    }

    @Override
    public void visitPercentPart(PercentExpensePart part) {
        BigDecimal amount = part.getPercent().multiply(percentTotal);
        result = result.add(amount);
    }

    @Override
    public void visitEqual(EqualExpensePartitioning partitioning) {
        int count = partitioning.getDebtors().size();
        BigDecimal totalDebt = partitioning.getTotal();
        BigDecimal debtPerDebtor = totalDebt.divide(BigDecimal.valueOf(count), RoundingMode.HALF_EVEN);

        result = result.add(debtPerDebtor);
    }

    @Override
    public void visitPercent(PercentExpensePartitioning partitioning) {
        percentTotal = partitioning.getTotal();
    }
}
