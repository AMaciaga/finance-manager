package com.gitlab.financemanager.services.debtcalc;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;

import java.math.BigDecimal;

/**
 * A service that allows to calculate debt among users in a wallet.
 *
 * @author Konrad Kleczkowski
 */
public interface DebtCalculator {
    /**
     * Calculates a debt between an issuer and a debtor in provided wallet.
     *
     * @param wallet the wallet
     * @param issuer the issuer of expenses
     * @param debtor the debtor
     * @return a total debt of <code>debtor</code> that owes to <code>issuer</code>
     * in <code>wallet</code>
     */
    BigDecimal totalDebtBetween(Wallet wallet, User issuer, User debtor);
}
