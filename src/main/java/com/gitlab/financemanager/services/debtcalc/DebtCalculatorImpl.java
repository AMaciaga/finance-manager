package com.gitlab.financemanager.services.debtcalc;

import com.gitlab.financemanager.entities.Expense;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;
import com.gitlab.financemanager.services.repos.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

/**
 * An implementation of {@link DebtCalculator}.
 *
 * @author Konrad Kleczkowski
 */
@Service
public class DebtCalculatorImpl implements DebtCalculator {
    private final ExpenseRepository expenseRepository;

    @Autowired
    public DebtCalculatorImpl(ExpenseRepository expenseRepository) {
        this.expenseRepository = expenseRepository;
    }

    @Override
    public BigDecimal totalDebtBetween(Wallet wallet, User issuer, User debtor) {
        Set<Expense> issuerExpenses = expenseRepository.findExpenses(wallet, issuer, debtor);
        Set<Expense> debtorExpenses = expenseRepository.findExpenses(wallet, debtor, issuer);

        DebtAccumulatingVisitor debtorDebtVisitor = new DebtAccumulatingVisitor();
        issuerExpenses.stream()
                .map(Expense::getExpensePartitioning)
                .forEach(partitioning -> partitioning.accept(debtorDebtVisitor));

        DebtAccumulatingVisitor issuerDebtVisitor = new DebtAccumulatingVisitor();
        debtorExpenses.stream()
                .map(Expense::getExpensePartitioning)
                .forEach(partitioning -> partitioning.accept(issuerDebtVisitor));

        BigDecimal totalDebt = debtorDebtVisitor.getResult();
        BigDecimal correction = issuerDebtVisitor.getResult();
        return totalDebt.subtract(correction);
    }
}
