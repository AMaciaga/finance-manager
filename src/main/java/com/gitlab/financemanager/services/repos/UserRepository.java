package com.gitlab.financemanager.services.repos;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

/**
 * A user JPA repository.
 *
 * @author Konrad Kleczkowski
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByPhoneNumber(String phoneNumber);

    @Query("select u " +
            "from app_users u, Expense e, ExpensePart part " +
            "where e.wallet = :wallet " +
            "and (e.issuer = u or (part.parent.parent = e and part.debtor = u))")
    Collection<User> findParticipants(@Param("wallet") Wallet wallet);
}
