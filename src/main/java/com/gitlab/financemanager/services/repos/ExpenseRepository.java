package com.gitlab.financemanager.services.repos;

import com.gitlab.financemanager.entities.Expense;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

/**
 * A user JPA repository.
 *
 * @author Weronika Ziemianek
 */
@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    /**
     * Selects expenses that <code>debtor</code> owes to <code>issuer</code>
     * in certain <code>wallet</code>.
     *
     * @param wallet the wallet
     * @param issuer the issuer
     * @param debtor the debtor
     * @return expenses where <code>debtor</code> owes to <code>issuer</code> in <code>wallet</code>
     */
    @Query("select e from Expense e " +
            "where e.wallet = :wallet " +
            "and e.issuer = :issuer " +
            "and (:debtor in (select p.debtor from ExpensePart p where p.parent = e.expensePartitioning) " +
            "or (:debtor in (select p.debtors from EqualExpensePartitioning p where e.expensePartitioning = p)))")
    Set<Expense> findExpenses(@Param("wallet") Wallet wallet,
                              @Param("issuer") User issuer,
                              @Param("debtor") User debtor);

    Optional<Expense> findByWalletAndName(Wallet wallet, String name);

    @Query("select e from Expense e, ExpensePart p, EqualExpensePartitioning eep " +
            "where e.wallet = :wallet and (e.issuer = :user " +
            "or (p.parent.parent = e and p.debtor = :user) " +
            "or (eep.parent = e and :user member of eep.debtors))")
    Collection<Expense> findParticipatedExpenses(@Param("wallet") Wallet wallet,
                                                 @Param("user") User user);
}
