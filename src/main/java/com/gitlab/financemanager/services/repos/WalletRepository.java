package com.gitlab.financemanager.services.repos;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Currency;
import java.util.List;
import java.util.Optional;

/**
 * A user JPA repository.
 *
 * @author Weronika Ziemianek
 */
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
    Optional<Wallet> findByName(String name);

    List<Wallet> findWalletsByName(String name);

    List<Wallet> findByCurrency(Currency currency);

    Optional<Wallet> findByOwnerAndName(User owner, String name);

    void deleteByOwnerAndName(User owner, String name);
}
