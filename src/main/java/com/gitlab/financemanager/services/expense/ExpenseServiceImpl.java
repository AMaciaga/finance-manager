package com.gitlab.financemanager.services.expense;

import com.gitlab.financemanager.controllers.DTO.ExpenseDTO;
import com.gitlab.financemanager.controllers.requests.ExpenseRequest;
import com.gitlab.financemanager.entities.Expense;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;
import com.gitlab.financemanager.services.repos.ExpenseRepository;
import com.gitlab.financemanager.services.repos.UserRepository;
import com.gitlab.financemanager.services.repos.WalletRepository;
import com.gitlab.financemanager.services.wallet.NoSuchWalletException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * An implementation of {@link ExpenseService}.
 *
 * @author Weronika Ziemianek
 * @author Konrad Kleczkowski
 */
@Service
public class ExpenseServiceImpl implements ExpenseService {
    private final UserRepository userRepository;
    private final WalletRepository walletRepository;
    private final ExpenseRepository expenseRepository;

    public ExpenseServiceImpl(UserRepository userRepository,
                              WalletRepository walletRepository,
                              ExpenseRepository expenseRepository) {
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
        this.expenseRepository = expenseRepository;
    }

    @Override
    public Long addExpense(Long walletId, User issuer, ExpenseRequest request)
            throws NoSuchWalletException, UserNotParticipatingException {
        Wallet wallet = walletRepository
                .findById(walletId)
                .filter(w -> w.getOwner().equals(issuer) || userRepository.findParticipants(w).contains(issuer))
                .orElseThrow(() -> new NoSuchWalletException(walletId));

        Expense expense = Expense
                .builder()
                .issueTime(LocalDateTime.now())
                .name(request.getName())
                .description(request.getDescription())
                .issuer(issuer)
                .wallet(wallet)
                .expensePartitioning(request.getExpensePartitioning())
//                .category(null)
                .build();

        wallet.getExpenses().add(expense);
        expenseRepository.saveAndFlush(expense);
        return expense.getId();
    }

    @Override
    public void editExpense(Long expenseId, User issuer, ExpenseRequest request)
            throws NoSuchExpenseException, UserNotParticipatingException {
        Expense expense = expenseRepository
                .findById(expenseId)
                .filter(e -> e.getIssuer().equals(issuer))
                .orElseThrow(() -> new NoSuchExpenseException(expenseId));

        Expense newExpense = expense.toBuilder()
                .name(request.getName())
                .expensePartitioning(request.getExpensePartitioning())
//                .category(null)
                .description(request.getDescription())
                .build();

        expenseRepository.saveAndFlush(newExpense);
    }

    @Override
    public void removeExpense(Long expenseId, User issuer)
            throws NoSuchExpenseException, UserNotParticipatingException {
        Expense expense = expenseRepository
                .findById(expenseId)
                .filter(e -> e.getIssuer().equals(issuer))
                .orElseThrow(() -> new NoSuchExpenseException(expenseId));

        expenseRepository.delete(expense);
    }

    @Override
    public Collection<ExpenseDTO> getExpenses(Long walletId, User user)
            throws NoSuchWalletException, UserNotParticipatingException {
        Wallet wallet = walletRepository
                .findById(walletId)
                .filter(w -> w.getOwner().equals(user) || userRepository.findParticipants(w).contains(user))
                .orElseThrow(() -> new NoSuchWalletException(walletId));

        return expenseRepository
                .findParticipatedExpenses(wallet, user)
                .stream()
                .map(this::getExpenseDTO)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public ExpenseDTO getExpenseById(User user, Long expenseId)
            throws NoSuchExpenseException, UserNotParticipatingException {
        Expense expense = expenseRepository
                .findById(expenseId)
                .filter(e -> userRepository.findParticipants(e.getWallet()).contains(user))
                .orElseThrow(() -> new NoSuchExpenseException(expenseId));

        return getExpenseDTO(expense);
    }

    @Override
    public Collection<ExpenseDTO> searchExpenses(Long walletId, String query, User user)
            throws NoSuchWalletException, UserNotParticipatingException {
        throw new UnsupportedOperationException();
    }

    private ExpenseDTO getExpenseDTO(Expense expense) {
        return ExpenseDTO.builder()
                .description(expense.getDescription())
                .expensePartitioning(expense.getExpensePartitioning())
                .issuer(expense.getIssuer().getPhoneNumber())
                .issueTime(expense.getIssueTime())
                .wallet(expense.getWallet().getId())
                .id(expense.getId())
                .build();
    }
}
