package com.gitlab.financemanager.services.expense;

/**
 * Thrown when user does not participate in wallet during operation.
 *
 * @author Konrad Kleczkowski
 */
public class UserNotParticipatingException extends Exception {
    public UserNotParticipatingException(String userName, String walletName) {
        super(String.format("User %s does not participate in wallet %s or does not owns the expense", userName, walletName));
    }
}
