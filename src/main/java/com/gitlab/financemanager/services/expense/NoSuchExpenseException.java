package com.gitlab.financemanager.services.expense;

public class NoSuchExpenseException extends Exception {
    public NoSuchExpenseException(Long expenseId) {
        super(String.format("Expense %d does not exist", expenseId));
    }
}
