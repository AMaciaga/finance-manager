package com.gitlab.financemanager.services.expense;

import com.gitlab.financemanager.controllers.DTO.ExpenseDTO;
import com.gitlab.financemanager.controllers.requests.ExpenseRequest;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.services.wallet.NoSuchWalletException;

import java.util.Collection;

/**
 * A service that allows to manage expenses in the database.
 *
 * @author Konrad Kleczkowski
 */
public interface ExpenseService {
    /**
     * Creates a new expense in the wallet.
     *
     * @param walletId the wallet ID
     * @param request  the request data
     * @return a new expense ID
     * @throws NoSuchWalletException         when wallet does not exist
     * @throws UserNotParticipatingException when user is not an owner of wallet
     *                                       or does not owe to another user in wallet
     */
    Long addExpense(Long walletId, User issuer, ExpenseRequest request) throws NoSuchWalletException, UserNotParticipatingException;

    /**
     * Edits an existing expense in the wallet.
     *
     * @param expenseId the expense ID
     * @param issuer    the user that issued this expense
     * @param request   the request data
     * @throws NoSuchExpenseException        when expense does not exist
     * @throws UserNotParticipatingException when user is not the issuer of the expense
     */
    void editExpense(Long expenseId, User issuer, ExpenseRequest request) throws NoSuchExpenseException, UserNotParticipatingException;

    /**
     * Removes an existing expense in the wallet.
     *
     * @param expenseId the expense ID
     * @param issuer    the user that issued this expense
     * @throws NoSuchExpenseException        when expense does not exist
     * @throws UserNotParticipatingException when user is not the issuer of the expense
     */
    void removeExpense(Long expenseId, User issuer) throws NoSuchExpenseException, UserNotParticipatingException;

    /**
     * Returns a list of expenses in which user has participated.
     *
     * @param walletId the wallet ID
     * @param user     the user that participates the wallet
     * @return the list of expenses that user participated in selected wallet
     */
    Collection<ExpenseDTO> getExpenses(Long walletId, User user) throws NoSuchWalletException, UserNotParticipatingException;

    /**
     * Returns an expense using its ID.
     *
     * @param expenseId the expense ID
     * @return the expense
     * @throws NoSuchExpenseException        when expense does not exist
     * @throws UserNotParticipatingException when user was not participating in the expense
     */
    ExpenseDTO getExpenseById(User user, Long expenseId) throws NoSuchExpenseException, UserNotParticipatingException;

    /**
     * Preforms a query on expenses in the wallet.
     *
     * @param walletId the wallet ID
     * @param query    the query
     * @param user     the user that participates the wallet
     * @return a list of results
     * @throws NoSuchWalletException         when wallet does not exist
     * @throws UserNotParticipatingException when user does not participate this wallet
     */
    Collection<ExpenseDTO> searchExpenses(Long walletId, String query, User user) throws NoSuchWalletException, UserNotParticipatingException;
}
