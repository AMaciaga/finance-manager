package com.gitlab.financemanager.services.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.gitlab.financemanager.entities.User;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserSerializer extends JsonSerializer<User> {
    @Override
    public void serialize(User value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(value.getPhoneNumber());
    }
}
