package com.gitlab.financemanager.services.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.services.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserDeserializer extends JsonDeserializer<User> {
    private final UserRepository repository;

    @Autowired
    public UserDeserializer(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return repository
                .findUserByPhoneNumber(p.getValueAsString())
                .orElseThrow(() -> new IOException("User cannot be deserialized because does not exist"));
    }
}
