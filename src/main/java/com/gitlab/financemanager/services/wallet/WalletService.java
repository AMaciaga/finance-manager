package com.gitlab.financemanager.services.wallet;

import com.gitlab.financemanager.controllers.DTO.WalletDTO;
import com.gitlab.financemanager.entities.User;

import java.util.Collection;
import java.util.Currency;

/**
 * A service that adds a new wallet to the database.
 *
 * @author Weronika Ziemianek
 * @author Konrad Kleczkowski
 */
public interface WalletService {
    /**
     * Adds a new wallet by an user.
     *
     * @param owner    the user that creates the wallet
     * @param name     the name of new wallet
     * @param currency the currency of new wallet
     * @throws WalletAlreadyExistsException when user owns a wallet with the same name as specified
     */
    Long addNewWallet(User owner, String name, Currency currency) throws WalletAlreadyExistsException;

    /**
     * Edits details of existing wallet.
     *
     * @param owner       the owner of existing wallet
     * @param id          the ID of existing wallet
     * @param newName     the new name of wallet, <code>null</code> if not specified
     * @param newCurrency the new currency of wallet, <code>null</code> if not specified
     * @throws NoSuchWalletException    when wallet with given name does not exist or user does not own it
     * @throws IllegalArgumentException if <code>newName</code> and <code>newCurrency</code> are both <code>null</code>
     */
    void editWallet(User owner, Long id, String newName, Currency newCurrency) throws NoSuchWalletException;

    /**
     * Removes an existing wallet.
     *
     * @param owner the owner of wallet
     * @param id    the ID of wallet
     * @throws NoSuchWalletException when wallet with given name does not exist or user does not own it
     */
    void removeWallet(User owner, Long id) throws NoSuchWalletException;

    /**
     * Gets a wallet by the name and the owner.
     *
     * @param owner the owner of wallet
     * @param id    the ID of wallet
     * @return a wallet
     * @throws NoSuchWalletException when wallet with given name does not exist or user does not own it
     */
    WalletDTO getWallet(User owner, Long id) throws NoSuchWalletException;

    /**
     * Returns a wallet lists that the user owns.
     *
     * @param user the user
     * @return all user's wallets
     */
    Collection<WalletDTO> getWalletList(User user);

    /**
     * Returns a search query result that uses some query to find wallets.
     *
     * @param query the query string that are used in search
     * @return the query result
     */
    Collection<WalletDTO> searchWallets(String query) throws IllegalWalletQueryException;
}
