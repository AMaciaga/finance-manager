package com.gitlab.financemanager.services.wallet;

/**
 * Thrown when the wallet already exists in the database.
 *
 * @author Weronika Ziemianek
 */
public class WalletAlreadyExistsException extends Exception {
    public WalletAlreadyExistsException(String name) {
        super(String.format("Wallet %s already exists", name));
    }
}
