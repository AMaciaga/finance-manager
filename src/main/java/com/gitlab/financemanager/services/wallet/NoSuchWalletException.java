package com.gitlab.financemanager.services.wallet;

import com.gitlab.financemanager.entities.User;

/**
 * Thrown when there is no such wallet in the database.
 *
 * @author Konrad Kleczkowski
 */
public class NoSuchWalletException extends Exception {
    /**
     * Creates an exception.
     *
     * @param owner the owner of wallet
     * @param name  the name of wallet
     */
    public NoSuchWalletException(User owner, String name) {
        super(String.format("User '%s' does not own wallet '%s'", owner.getPhoneNumber(), name));
    }

    /**
     * Creates an exception.
     *
     * @param id the ID of wallet
     */
    public NoSuchWalletException(Long id) {
        super(String.format("Wallet with ID %d does not exist", id));
    }
}
