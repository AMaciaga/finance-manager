package com.gitlab.financemanager.services.wallet;

/**
 * Thrown when wallet search query is ill-formed.
 *
 * @author Konrad Kleczkowski
 */
public class IllegalWalletQueryException extends Exception {
    /**
     * Creates an exception.
     *
     * @param message the error message
     */
    public IllegalWalletQueryException(String message) {
        super(message);
    }
}
