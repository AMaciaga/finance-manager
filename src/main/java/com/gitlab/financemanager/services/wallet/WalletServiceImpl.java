package com.gitlab.financemanager.services.wallet;


import com.gitlab.financemanager.controllers.DTO.WalletDTO;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.entities.Wallet;
import com.gitlab.financemanager.services.repos.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * An implementation of {@link WalletService}
 *
 * @author Weronika Ziemianek
 */
@Service
public class WalletServiceImpl implements WalletService {
    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @Override
    public Long addNewWallet(User owner, String name, Currency currency) throws WalletAlreadyExistsException {
        if (walletRepository.findByOwnerAndName(owner, name).isPresent()) {
            throw new WalletAlreadyExistsException(name);
        }
        Wallet wallet = Wallet.builder()
                .owner(owner)
                .name(name)
                .currency(currency)
                .build();
        walletRepository.saveAndFlush(wallet);
        return wallet.getId();
    }

    @Override
    public void editWallet(User owner, Long id, String newName, Currency newCurrency) throws NoSuchWalletException {
        if (newName == null && newCurrency == null) throw new IllegalArgumentException();
        Wallet newWallet = walletRepository
                .findById(id)
                .filter(w -> w.getOwner().equals(owner))
                .orElseThrow(() -> new NoSuchWalletException(owner, id.toString()))
                .toBuilder()
                .name(newName)
                .currency(newCurrency)
                .build();
        walletRepository.saveAndFlush(newWallet);
    }

    @Override
    public void removeWallet(User owner, Long id) throws NoSuchWalletException {
        Wallet wallet = walletRepository
                .findById(id)
                .filter(w -> w.getOwner().equals(owner))
                .orElseThrow(() -> new NoSuchWalletException(owner, id.toString()));
        walletRepository.delete(wallet);
    }

    @Override
    public WalletDTO getWallet(User owner, Long id) throws NoSuchWalletException {
        Wallet wallet = walletRepository
                .findById(id)
                .filter(w -> w.getOwner().equals(owner))
                .orElseThrow(() -> new NoSuchWalletException(owner, id.toString()));
        return getWalletDto(wallet);
    }

    @Override
    public Collection<WalletDTO> getWalletList(User user) {
        return user
                .getWallets()
                .stream()
                .map(this::getWalletDto)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public Collection<WalletDTO> searchWallets(String query) throws IllegalWalletQueryException {
        throw new UnsupportedOperationException();
    }

    private WalletDTO getWalletDto(Wallet wallet) {
        return WalletDTO
                .builder()
                .id(wallet.getId())
                .currency(wallet.getCurrency())
                .name(wallet.getName())
                .owner(wallet.getOwner())
                .build();
    }
}
