package com.gitlab.financemanager.entities;

import com.gitlab.financemanager.entities.auth.SimpleUserDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Objects;

/**
 * An user entity.
 *
 * @author Konrad Kleczkowski
 */
@Entity(name = "app_users")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {
    /**
     * An unique user ID.
     */
    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    /**
     * A phone number.
     */
    @Pattern(regexp = "^\\+?\\d{10,14}$", message = "Invalid phone number")
    @Column(unique = true)
    @EqualsAndHashCode.Include
    private String phoneNumber;

    /**
     * A nickname of this user.
     */
    @NotEmpty(message = "Nickname should be non-empty")
    private String nickName;

    /**
     * Wallets that belongs to this user.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner", orphanRemoval = true)
    private Collection<Wallet> wallets;

    /**
     * Expenses that were issued by this user.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "issuer", orphanRemoval = true)
    private Collection<Expense> expenses;

    /**
     * A Spring Security user detail.
     */
    @OneToOne(cascade = CascadeType.ALL, targetEntity = SimpleUserDetails.class)
    private UserDetails userDetails;

    /**
     * @deprecated This constructor should be used only by JPA.
     */
    @Deprecated
    public User() {
    }
}
