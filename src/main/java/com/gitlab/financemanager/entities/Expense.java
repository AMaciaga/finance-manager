package com.gitlab.financemanager.entities;

import com.gitlab.financemanager.entities.expense.ExpensePartitioning;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * An entity that models expense.
 * <p>
 * Each expense has an issuer, a debtor and amount of debt.
 * Expense is mapped to some wallet.
 * <p>
 * Expense is divided into parts. The way of obtaining
 * parts are called partitioning. See {@link ExpensePartitioning}
 * for further information.
 *
 * @author Konrad Kleczkowski
 */
@Entity
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"wallet"})
public class Expense {
    /**
     * An unique expense ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * An associated wallet to this expense.
     * Should be not null.
     */
    @ManyToOne
    private Wallet wallet;

    /**
     * A timestamp when this expense were issued. Should be not null.
     */
    @NotNull(message = "Issue time should be specified")
    private LocalDateTime issueTime;

    /**
     * The issuer of this expense.
     * Should be not null.
     */
    @ManyToOne
    private User issuer;

    /**
     * A name of expense.
     */
    @NotEmpty(message = "Expense name should be not empty")
    private String name;

    /**
     * A partitioning of this expense.
     */
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "parent")
    private ExpensePartitioning expensePartitioning;

    /**
     * A description of expense.
     */
    private String description;

    /**
     * The category of expense. Could be null (expense is uncategorized).
     */
    @ManyToOne
    private ExpenseCategory category;
}
