package com.gitlab.financemanager.entities.auth;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * An authority entity. Keeps associated authorities to the {@link UserDetails user details}.
 *
 * @author Konrad Kleczkowski
 * @see GrantedAuthority
 */
@Entity
@Data
public class SimpleGrantedAuthority implements GrantedAuthority {
    /**
     * An authority ID.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * An authority key.
     */
    private String authority;

    /**
     * Creates an empty authority.
     *
     * @deprecated Only Java Persistence API should use this constructor.
     */
    @Deprecated
    public SimpleGrantedAuthority() {
    }

    /**
     * Creates an authority.
     *
     * @param authority the key of authority kept as string
     */
    public SimpleGrantedAuthority(String authority) {
        this.authority = authority;
    }
}
