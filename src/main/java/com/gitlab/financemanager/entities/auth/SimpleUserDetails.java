package com.gitlab.financemanager.entities.auth;

import com.gitlab.financemanager.entities.User;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;

/**
 * An {@link UserDetails} implementation for OTP authentication.
 *
 * @author Konrad Kleczkowski
 */
@Entity
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode(exclude = {"user"})
public class SimpleUserDetails implements UserDetails {
    /**
     * User details ID.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * A collection of granted authorities for an user.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<SimpleGrantedAuthority> authorities;

    /**
     * An user that is subject of these details.
     */
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userDetails")
    private User user;

    /**
     * Checks whether an account is enabled.
     */
    @Builder.Default
    private boolean enabled = true;

    /**
     * Checks whether an account is not locked.
     */
    @Builder.Default
    private boolean accountNonLocked = true;

    /**
     * A password. Used as a salt to OTP secret key.
     */
    private String password;

    /**
     * Creates an empty {@link SimpleUserDetails}.
     *
     * @deprecated Only Java Persistence API should use this constructor.
     */
    @Deprecated
    public SimpleUserDetails() {
    }

    /**
     * Returns a phone number from {@link User}.
     *
     * @return phone number as an username
     */
    @Transient
    @Override
    public String getUsername() {
        return user.getPhoneNumber();
    }

    /**
     * Returns <code>true</code>.
     */
    @Transient
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Returns <code>true</code>.
     */
    @Transient
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
}
