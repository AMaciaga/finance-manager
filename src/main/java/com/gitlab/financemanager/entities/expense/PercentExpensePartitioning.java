package com.gitlab.financemanager.entities.expense;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * A partitioning that divides expense using percent shares.
 *
 * @author Konrad Kleczkowski
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class PercentExpensePartitioning extends ExpensePartitioning {
    /**
     * The amount of money to divide among each debtor in part.
     */
    @Column(scale = 2)
    private BigDecimal total;

    /**
     * Parts of this partitioning.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true)
    private Collection<PercentExpensePart> parts;

    @Override
    public void accept(ExpenseVisitor visitor) {
        visitor.visitPercent(this);
        for (PercentExpensePart part : parts) {
            visitor.visitPercentPart(part);
        }
    }
}
