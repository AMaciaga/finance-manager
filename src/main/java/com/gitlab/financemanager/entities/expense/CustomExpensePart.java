package com.gitlab.financemanager.entities.expense;

import com.gitlab.financemanager.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * A part of {@link CustomExpensePartitioning}. Contains exact amount of money
 * for some {@link User debtor}.
 *
 * @author Konrad Kleczkowski
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CustomExpensePart extends ExpensePart {
    /**
     * Amount of money that debtor owes.
     */
    @Column(scale = 2)
    private BigDecimal amount;
}
