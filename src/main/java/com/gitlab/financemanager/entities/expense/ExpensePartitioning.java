package com.gitlab.financemanager.entities.expense;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.gitlab.financemanager.entities.Expense;
import lombok.Data;

import javax.persistence.*;

/**
 * An abstract entity that indicates the way of expense partitioning.
 * <p>
 * Partitioning stands with expense in one-to-one relationship.
 *
 * @author Konrad Kleczkowski
 * @see Expense
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EqualExpensePartitioning.class, name = "equal"),
        @JsonSubTypes.Type(value = PercentExpensePartitioning.class, name = "percent"),
        @JsonSubTypes.Type(value = CustomExpensePartitioning.class, name = "custom")
})
@Entity
@Inheritance
@Data
public abstract class ExpensePartitioning {
    /**
     * An unique ID of partitioning.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Parent expense.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private Expense parent;

    /**
     * Walks through expense parts using provided visitor.
     *
     * @param visitor a visitor instance, must be non-null
     * @throws NullPointerException if <code>visitor</code> is null
     */
    public abstract void accept(ExpenseVisitor visitor);
}
