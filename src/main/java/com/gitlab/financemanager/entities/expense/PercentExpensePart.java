package com.gitlab.financemanager.entities.expense;

import com.gitlab.financemanager.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * A part of {@link PercentExpensePartitioning}. Contains percent that reflects
 * a share of some {@link User debtor}.
 *
 * @author Konrad Kleczkowski
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class PercentExpensePart extends ExpensePart {
    @Column(scale = 2, precision = 3)
    private BigDecimal percent;
}
