package com.gitlab.financemanager.entities.expense;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.financemanager.entities.ExpenseCategory;
import com.gitlab.financemanager.entities.User;
import lombok.Data;

import javax.persistence.*;

/**
 * A part of expense. This entity is aggregated by {@link ExpensePartitioning}s
 * if they need more sophisticated way for expressing expense parts.
 *
 * @author Konrad Kleczkowski
 */
@Entity
@Inheritance
@Data
public abstract class ExpensePart {
    /**
     * An unique expense part ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The debtor of this expense part.
     */
    @ManyToOne
    private User debtor;

    /**
     * Parent expense partitioning.
     */
    @ManyToOne
    @JsonIgnore
    private ExpensePartitioning parent;

    /**
     * A category of expense part.
     */
    @ManyToOne
    private ExpenseCategory category;

    /**
     * A description.
     */
    private String description;
}
