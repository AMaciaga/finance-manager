package com.gitlab.financemanager.entities.expense;

import com.gitlab.financemanager.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

/**
 * A way of partitioning that each part is an exact amount of money
 * for each {@link User debtor}.
 *
 * @author Konrad Kleczkowski
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CustomExpensePartitioning extends ExpensePartitioning {
    /**
     * A custom expense parts.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true)
    private Collection<CustomExpensePart> parts;

    @Override
    public void accept(ExpenseVisitor visitor) {
        visitor.visitCustom(this);
        for (CustomExpensePart part : parts) {
            visitor.visitCustomPart(part);
        }
    }
}
