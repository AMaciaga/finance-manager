package com.gitlab.financemanager.entities.expense;

import com.gitlab.financemanager.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Set;

/**
 * A way of partitioning that each {@link User debtor}
 * owes the same amount of money.
 *
 * @author Konrad Kleczkowski
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class EqualExpensePartitioning extends ExpensePartitioning {
    /**
     * A debtors set.
     */
    @OneToMany
    private Set<User> debtors;

    /**
     * Total amount of money to divide among each debtor.
     */
    @Column(scale = 2)
    private BigDecimal total;

    @Override
    public void accept(ExpenseVisitor visitor) {
        visitor.visitEqual(this);
    }
}
