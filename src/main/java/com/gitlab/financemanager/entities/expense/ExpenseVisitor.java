package com.gitlab.financemanager.entities.expense;

/**
 * A visitor that walks through expense parts.
 *
 * @author Konrad Kleczkowski
 */
public interface ExpenseVisitor {
    /**
     * Visits a part of {@link CustomExpensePartitioning}.
     *
     * @param part an instance of a part
     * @implNote Default implementation does nothing
     */
    default void visitCustomPart(CustomExpensePart part) {

    }

    /**
     * Visits a part of {@link PercentExpensePartitioning}.
     *
     * @param part an instance of a part
     * @implNote Default implementation does nothing
     */
    default void visitPercentPart(PercentExpensePart part) {

    }

    /**
     * Visits an {@link EqualExpensePartitioning}.
     *
     * @param partitioning an instance of a partitioning
     * @implNote Default implementation does nothing
     */
    default void visitEqual(EqualExpensePartitioning partitioning) {

    }

    /**
     * Visits a {@link PercentExpensePartitioning}.
     *
     * @param partitioning an instance of a partitioning
     * @implNote Default implementation does nothing
     */
    default void visitPercent(PercentExpensePartitioning partitioning) {

    }

    /**
     * Visits a {@link CustomExpensePartitioning}.
     *
     * @param partitioning an instance of a partitioning
     * @implNote Default implementation does nothing
     */
    default void visitCustom(CustomExpensePartitioning partitioning) {

    }
}
