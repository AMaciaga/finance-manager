package com.gitlab.financemanager.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

/**
 * An entity that reassembles categories of expense.
 *
 * @author Konrad Kleczkowski
 */
@Entity
@Data
public class ExpenseCategory {
    /**
     * An unique expense category ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The category name of expense.
     */
    @NotEmpty(message = "The category name should not be empty")
    private String name;
}
