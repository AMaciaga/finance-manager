package com.gitlab.financemanager.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Currency;
import java.util.Set;

/**
 * An entity that reflects a wallet.
 * <p>
 * A wallet is logical group of expenses that users
 * owes debts to each other.
 *
 * @author Konrad Kleczkowski
 */
@Entity
@Data
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(AccessLevel.PACKAGE)
@EqualsAndHashCode(exclude = {"expenses"})
public class Wallet {
    /**
     * An unique wallet ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The owner of this wallet.
     */
    @ManyToOne
    private User owner;

    /**
     * The name of this wallet.
     */
    @NotEmpty(message = "The name of wallet should be non-empty")
//    @Column(unique = true)
    private String name;

    /**
     * The currency of expenses.
     */
    @NotNull(message = "Currency should be specified")
    private Currency currency;

    /**
     * Expenses that this wallet contains.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wallet", orphanRemoval = true)
    private Set<Expense> expenses;

    /**
     * @deprecated Only JPA should use this constructor.
     */
    @Deprecated
    public Wallet() {
    }
}
