package com.gitlab.financemanager.configs;

import com.gitlab.financemanager.services.auth.OtpAuthenticationProvider;
import com.gitlab.financemanager.services.auth.jwt.JwtAuthenticationFilter;
import com.gitlab.financemanager.services.auth.jwt.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.session.ConcurrentSessionFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * A configuration of Spring Security that uses
 * one-time password sent via SMS service utility
 * and JWT tokens to achieve authentication layer.
 * <p>
 * To provide secret key for JWT signing set
 * <code>security.jwt.secret</code> environmental variable.
 *
 * @author Konrad Kleczkowski
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final OtpAuthenticationProvider authenticationProvider;
    private final Environment env;

    @Value("${security.jwt.secret}")
    private String jwtSecretKey;

    @Autowired
    public SecurityConfig(OtpAuthenticationProvider authenticationProvider, Environment env) {
        this.authenticationProvider = authenticationProvider;
        this.env = env;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO: Specify auth-free matchers
        http.cors()
                .and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/public/**").permitAll()
                .antMatchers("/auth/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterAfter(new JwtAuthenticationFilter(authenticationManager(), jwtSecretKey), ConcurrentSessionFilter.class)
                .addFilterAfter(new JwtAuthorizationFilter(authenticationManager(), jwtSecretKey), JwtAuthenticationFilter.class)
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        if (env.acceptsProfiles(Profiles.of("dev"))) http.headers().frameOptions().disable();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource configurationSource = new UrlBasedCorsConfigurationSource();
        configurationSource.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return configurationSource;
    }
}
