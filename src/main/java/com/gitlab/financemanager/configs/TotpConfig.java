package com.gitlab.financemanager.configs;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.NoSuchAlgorithmException;
import java.time.Duration;

@Configuration
public class TotpConfig {
    @Bean
    public TimeBasedOneTimePasswordGenerator passwordGenerator() {
        try {
            return new TimeBasedOneTimePasswordGenerator(Duration.ofSeconds(30), 8);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }
}
