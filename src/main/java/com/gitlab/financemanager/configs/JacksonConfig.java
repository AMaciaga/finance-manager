package com.gitlab.financemanager.configs;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.services.serialization.UserDeserializer;
import com.gitlab.financemanager.services.serialization.UserSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * A serializer configuration.
 *
 * @author Konrad Kleczkowski
 */
@Configuration
public class JacksonConfig {
    private final UserSerializer userSerializer;
    private final UserDeserializer userDeserializer;

    @Autowired
    public JacksonConfig(UserSerializer userSerializer, UserDeserializer userDeserializer) {
        this.userSerializer = userSerializer;
        this.userDeserializer = userDeserializer;
    }

    @Bean
    public Module module() {
        return new SimpleModule()
                .addSerializer(User.class, userSerializer)
                .addDeserializer(User.class, userDeserializer);
    }
}
