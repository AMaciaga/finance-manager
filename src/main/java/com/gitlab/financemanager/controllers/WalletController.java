package com.gitlab.financemanager.controllers;

import com.gitlab.financemanager.controllers.DTO.WalletDTO;
import com.gitlab.financemanager.controllers.requests.WalletRequest;
import com.gitlab.financemanager.entities.auth.SimpleUserDetails;
import com.gitlab.financemanager.services.wallet.IllegalWalletQueryException;
import com.gitlab.financemanager.services.wallet.NoSuchWalletException;
import com.gitlab.financemanager.services.wallet.WalletAlreadyExistsException;
import com.gitlab.financemanager.services.wallet.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

/**
 * A controller that interfaces {@link WalletService}.
 *
 * @author Weronika Ziemianek
 * @author Konrad Kleczkowski
 */
@RestController
public class WalletController {
    private final WalletService walletService;

    @Autowired
    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    /**
     * Adds a new wallet to an authenticated user.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when wallet has been added successfully;</li>
     *     <li><code>409 Conflict</code> when wallet already exists.</li>
     * </ul>
     *
     * @param request   the request body
     * @param principal an authentication principal
     * @return a new wallet ID
     */
    @PostMapping("/wallet")
    public Long add(@RequestBody WalletRequest request,
                    @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            return walletService.addNewWallet(principal.getUser(), request.getName(), request.getCurrency());
        } catch (WalletAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Wallet already exists", e);
        }
    }

    /**
     * Edits details about existing wallet.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when changes has been committed successfully;</li>
     *     <li><code>400 Bad request</code> when there is not new name or new currency to edit</li>
     *     <li><code>404 Not Found</code> if given wallet does not exist.</li>
     * </ul>
     *
     * @param id        a wallet ID
     * @param request   the request body
     * @param principal an authentication principal
     */
    @PutMapping("/wallet/{id}")
    public void edit(@PathVariable("id") Long id,
                     @RequestBody WalletRequest request,
                     @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            walletService.editWallet(
                    principal.getUser(),
                    id,
                    request.getName(),
                    request.getCurrency());
        } catch (NoSuchWalletException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Wallet does not exist", e);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nothing to edit in existing wallet", e);
        }
    }

    /**
     * Removes an existing wallet from an user.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when removal has been done successfully;</li>
     *     <li><code>404 Not found</code> when given wallet does not exist.</li>
     * </ul>
     *
     * @param id        a wallet ID
     * @param principal an authentication principal
     */
    @DeleteMapping("/wallet/{id}")
    public void remove(@PathVariable("id") Long id,
                       @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            walletService.removeWallet(principal.getUser(), id);
        } catch (NoSuchWalletException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Wallet does not exist", e);
        }
    }

    /**
     * Returns a wallet using its name.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when wallets exists in database and user owns it;</li>
     *     <li><code>404 Not found</code> when wallets does not exist or the user does not owns it.</li>
     * </ul>
     *
     * @param id        a wallet ID
     * @param principal an authentication principal
     * @return a wallet that matches given name
     */
    @GetMapping("/wallet/{id}")
    public WalletDTO getWallet(@PathVariable("id") Long id,
                               @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            return walletService.getWallet(principal.getUser(), id);
        } catch (NoSuchWalletException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Wallet does not exist", e);
        }
    }

    /**
     * Returns a list of user's wallet.
     *
     * @param principal an authentication principal
     * @return a list of wallets that user owns
     */
    @GetMapping("/wallet/all")
    public Collection<WalletDTO> getAllWallets(@AuthenticationPrincipal SimpleUserDetails principal) {
        return walletService.getWalletList(principal.getUser());
    }

    /**
     * Seeks for a wallet using its name.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when query has been preformed successfully.
     *     Response payload contains a list of wallets that conforms the query.</li>
     *     <li><code>400 Bad request</code> when query is an empty string.</li>
     * </ul>
     *
     * @param query the query for searching
     * @return a result as a collection of wallets
     */
    @GetMapping("/wallet/search")
    public Collection<WalletDTO> searchWallet(@RequestParam("query") String query) {
        try {
            return walletService.searchWallets(query);
        } catch (IllegalWalletQueryException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Query string is invalid", e);
        }
    }
}
