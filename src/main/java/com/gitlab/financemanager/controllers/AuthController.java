package com.gitlab.financemanager.controllers;

import com.gitlab.financemanager.controllers.requests.RegisterUserRequest;
import com.gitlab.financemanager.services.auth.AuthService;
import com.gitlab.financemanager.services.auth.NoSuchUserToChallengeException;
import com.gitlab.financemanager.services.auth.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * A controller that interfaces {@link AuthService}.
 *
 * @author Konrad Kleczkowski
 */
@RestController
public class AuthController {
    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    /**
     * Registers a new user into the database.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when registration has been done successfully;</li>
     *     <li><code>409 Conflict</code> when an user already exists in the database.</li>
     * </ul>
     *
     * @param request a request body
     */
    @PostMapping("/auth/register")
    public void register(@Valid @RequestBody RegisterUserRequest request) {
        try {
            authService.register(request.getPhoneNumber(), request.getNickName());
        } catch (UserAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User already exists.", e);
        }
    }

    /**
     * Begins a challenge procedure for given user.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> if procedure has been done properly;</li>
     *     <li><code>404 Not Found</code> if an user does not exist.</li>
     * </ul>
     *
     * @param phoneNumber the phone number that identifies the user
     */
    @GetMapping("/auth/challenge")
    public void challenge(@RequestParam("phoneNumber") String phoneNumber) {
        try {
            authService.challenge(phoneNumber);
        } catch (NoSuchUserToChallengeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist.", e);
        }
    }
}
