package com.gitlab.financemanager.controllers.DTO;

import com.gitlab.financemanager.entities.User;
import lombok.Builder;
import lombok.Data;

import java.util.Currency;

@Data
@Builder
public class WalletDTO {
    private Long id;
    private User owner;
    private String name;
    private Currency currency;
//    private Set<Expense> expenses;
}


