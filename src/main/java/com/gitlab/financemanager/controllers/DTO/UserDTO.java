package com.gitlab.financemanager.controllers.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {
    private String phoneNumber;
    //    private String password;
    private String nickName;
//    private Collection<Wallet> wallets;
//    private Collection<Expense> expenses;
    // private Collection<Expense> debts;
}


