package com.gitlab.financemanager.controllers.DTO;

import com.gitlab.financemanager.entities.expense.ExpensePartitioning;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ExpenseDTO {
    private Long id;
    private Long wallet;
    private LocalDateTime issueTime;
    private String issuer;
    private ExpensePartitioning expensePartitioning;
    private String description;
    private String category;
}


