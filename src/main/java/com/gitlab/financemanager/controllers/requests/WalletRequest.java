package com.gitlab.financemanager.controllers.requests;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Currency;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class WalletRequest {
    private String name;
    private Currency currency;

    /**
     * @deprecated Jackson should use this constructor only.
     */
    @Deprecated
    public WalletRequest() {
    }
}
