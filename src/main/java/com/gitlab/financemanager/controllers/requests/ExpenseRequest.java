package com.gitlab.financemanager.controllers.requests;

import com.gitlab.financemanager.entities.expense.ExpensePartitioning;
import lombok.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ExpenseRequest {
    private String name;
    private String description;
    private ExpensePartitioning expensePartitioning;
    private String category;

    /**
     * @deprecated Jackson should use this constructor only.
     */
    @Deprecated
    public ExpenseRequest() {
    }
}
