package com.gitlab.financemanager.controllers.requests;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class RegisterUserRequest {
    /**
     * A phone number.
     */
    @Pattern(regexp = "^\\+?\\d{10,14}$", message = "Invalid phone number")
    private String phoneNumber;

    private String nickName;

    /**
     * @deprecated Jackson should use this constructor only.
     */
    @Deprecated
    public RegisterUserRequest() {
    }
}
