package com.gitlab.financemanager.controllers.requests;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class UserRequest {
    private String username;

    /**
     * @deprecated Jackson should use this constructor only.
     */
    @Deprecated
    public UserRequest() {
    }
}
