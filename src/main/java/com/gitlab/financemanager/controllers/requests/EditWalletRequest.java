package com.gitlab.financemanager.controllers.requests;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Currency;

@Data
public class EditWalletRequest {
    private String name;
    private String newName;
    private Currency newCurrency;

    /**
     * @deprecated Jackson should use this constructor only.
     */
    @Deprecated
    public EditWalletRequest() {
    }
}
