package com.gitlab.financemanager.controllers;

import com.gitlab.financemanager.controllers.DTO.ExpenseDTO;
import com.gitlab.financemanager.controllers.requests.ExpenseRequest;
import com.gitlab.financemanager.entities.auth.SimpleUserDetails;
import com.gitlab.financemanager.services.expense.ExpenseService;
import com.gitlab.financemanager.services.expense.NoSuchExpenseException;
import com.gitlab.financemanager.services.expense.UserNotParticipatingException;
import com.gitlab.financemanager.services.wallet.NoSuchWalletException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
public class ExpenseController {
    private final ExpenseService expenseService;

    @Autowired
    public ExpenseController(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    /**
     * Adds new expense to the wallet.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when adding was done successfully;</li>
     *     <li><code>404 Not found</code> when wallet does not exist;</li>
     *     <li><code>403 Forbidden</code> when user does not participate in the wallet.</li>
     * </ul>
     *
     * @param walletId  the wallet ID
     * @param request   the request body
     * @param principal an authentication principal
     * @return a new expense ID
     */
    @PostMapping("/wallet/{walletId}")
    public Long add(@PathVariable("walletId") Long walletId,
                    @RequestBody ExpenseRequest request,
                    @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            return expenseService.addExpense(walletId, principal.getUser(), request);
        } catch (NoSuchWalletException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Wallet does not exist", e);
        } catch (UserNotParticipatingException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not participating in the wallet", e);
        }
    }

    /**
     * Edits details about existing expense.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when editing was done successfully;</li>
     *     <li><code>404 Not found</code> when wallet does not exist;</li>
     *     <li><code>403 Forbidden</code> when user does not own the wallet.</li>
     * </ul>
     *
     * @param expense   the expense ID
     * @param request   the request body
     * @param principal an authentication principal
     */
    @PutMapping("/expense/{expenseId}")
    public void edit(@PathVariable("expenseId") Long expense,
                     @RequestBody ExpenseRequest request,
                     @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            expenseService.editExpense(expense, principal.getUser(), request);
        } catch (NoSuchExpenseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Expense does not exist", e);
        } catch (UserNotParticipatingException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not owning this expense", e);
        }
    }

    /**
     * Removes an existing expense.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when removing was done successfully;</li>
     *     <li><code>404 Not found</code> when wallet does not exist;</li>
     *     <li><code>403 Forbidden</code> when user does not own the wallet.</li>
     * </ul>
     *
     * @param expense   the expense ID
     * @param principal an authentication principal
     */
    @DeleteMapping("/expense/{expenseId}")
    public void remove(@PathVariable("expenseId") Long expense,
                       @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            expenseService.removeExpense(expense, principal.getUser());
        } catch (NoSuchExpenseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Expense does not exist", e);
        } catch (UserNotParticipatingException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not owning this expense", e);
        }
    }

    /**
     * Gets an expense by ID.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when expense was returned successfully;</li>
     *     <li><code>404 Not found</code> when expense does not exist;</li>
     *     <li><code>403 Forbidden</code> when user does not own the wallet.</li>
     * </ul>
     *
     * @param expenseId the expense ID
     * @param principal an authentication principal
     * @return the expense
     */
    @GetMapping("/expense/{expenseId}")
    public ExpenseDTO getById(@PathVariable Long expenseId,
                              @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            return expenseService.getExpenseById(principal.getUser(), expenseId);
        } catch (NoSuchExpenseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Expense does not exist", e);
        } catch (UserNotParticipatingException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not an issuer or a debtor of expense", e);
        }
    }

    /**
     * Gets all expenses in the wallet.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when expenses were returned successfully;</li>
     *     <li><code>404 Not found</code> when wallet does not exist;</li>
     *     <li><code>403 Forbidden</code> when user does not own the wallet.</li>
     * </ul>
     *
     * @param walletId  the wallet ID
     * @param principal an authentication principal
     * @return the expense
     */
    @GetMapping("/wallet/{walletId}/all")
    public Collection<ExpenseDTO> getAll(@PathVariable("walletId") Long walletId,
                                         @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            return expenseService.getExpenses(walletId, principal.getUser());
        } catch (NoSuchWalletException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Wallet does not exist", e);
        } catch (UserNotParticipatingException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    "User is not an issuer or a debtor in wallet's expenses", e);
        }
    }

    /**
     * Search all expenses in the wallet.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when expenses were returned successfully;</li>
     *     <li><code>404 Not found</code> when wallet does not exist;</li>
     *     <li><code>403 Forbidden</code> when user does not own the wallet.</li>
     * </ul>
     *
     * @param walletId  the wallet ID
     * @param query     the query
     * @param principal an authentication principal
     * @return the expense
     */
    @GetMapping("/wallet/{walletId}/search")
    public Collection<ExpenseDTO> search(@PathVariable("walletId") Long walletId,
                                         @RequestParam("query") String query,
                                         @AuthenticationPrincipal SimpleUserDetails principal) {
        try {
            return expenseService.searchExpenses(walletId, query, principal.getUser());
        } catch (NoSuchWalletException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Wallet does not exist", e);
        } catch (UserNotParticipatingException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    "User is not an issuer or a debtor in wallet's expenses", e);
        }
    }
}
