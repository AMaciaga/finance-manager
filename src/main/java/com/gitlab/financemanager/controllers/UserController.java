package com.gitlab.financemanager.controllers;

import com.gitlab.financemanager.controllers.requests.UserRequest;
import com.gitlab.financemanager.entities.auth.SimpleUserDetails;
import com.gitlab.financemanager.services.auth.AuthService;
import com.gitlab.financemanager.services.auth.NoSuchUserToChallengeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * A controller that interfaces {@link AuthService}.
 *
 * @author Weronika Ziemianek
 */

@RestController
public class UserController {
    private final AuthService authService;

    @Autowired
    public UserController(AuthService authService){this.authService = authService;}

    /**
     * Edits username.
     * <p>
     * Returns:
     * <ul>
     *     <li><code>200 OK</code> when changes has been committed successfully;</li>
     *     <li><code>400 Bad request</code> when the username is not new</li>
     *     <li><code>404 Not Found</code> if given user does not exist.</li>
     * </ul>
     *
     * @param id        a user ID
     * @param request   the request body
     * @param principal an authentication principal
     */

    @PutMapping("/user/{id}")
    public void edit(@PathVariable("id") Long id,
                     @RequestBody UserRequest request,
                     @AuthenticationPrincipal SimpleUserDetails principal){

        try {
            authService.editUser(
                    id,
                    request.getUsername()
            );

        } catch (NoSuchUserToChallengeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist", e);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nothing to edit", e);
        }
    }
}
