package com.gitlab.financemanager.controllers;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import com.gitlab.financemanager.services.repos.UserRepository;
import com.gitlab.financemanager.services.auth.UserAlreadyExistsException;
import com.gitlab.financemanager.services.auth.AuthServiceImpl;
import com.gitlab.financemanager.services.auth.otp.DummyUserChallenger;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.security.NoSuchAlgorithmException;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRegistrationTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void registerUserTest() throws NoSuchAlgorithmException, UserAlreadyExistsException {
        TimeBasedOneTimePasswordGenerator passwordGenerator = new TimeBasedOneTimePasswordGenerator();
        DummyUserChallenger userChallenger = new DummyUserChallenger(passwordGenerator);

        AuthServiceImpl userRegisterService = new AuthServiceImpl(userRepository, userChallenger);

        String phoneNumber = "+48123456789";
        String nickname = "ABCD";
        userRegisterService.register(phoneNumber, nickname);

        assert userRepository.findUserByPhoneNumber(phoneNumber).isPresent();
    }

    @Test
    public void registerTwoSameUsersTestExpectedException() throws NoSuchAlgorithmException, UserAlreadyExistsException {
        TimeBasedOneTimePasswordGenerator passwordGenerator = new TimeBasedOneTimePasswordGenerator();
        DummyUserChallenger userChallenger = new DummyUserChallenger(passwordGenerator);

        AuthServiceImpl userRegisterService = new AuthServiceImpl(userRepository, userChallenger);

        String phoneNumber = "+48123456789";
        String nickname = "ABCD";
        userRegisterService.register(phoneNumber, nickname);

        Assertions.assertThrows(UserAlreadyExistsException.class, () -> {
            userRegisterService.register(phoneNumber, nickname);
        });
    }

    @Test
    public void wrongNumberTestExpectedException() throws NoSuchAlgorithmException {
        TimeBasedOneTimePasswordGenerator passwordGenerator = new TimeBasedOneTimePasswordGenerator();
        DummyUserChallenger userChallenger = new DummyUserChallenger(passwordGenerator);

        AuthServiceImpl userRegisterService = new AuthServiceImpl(userRepository, userChallenger);

        String phoneNumber = "+48";
        String nickname = "ABCD";

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            userRegisterService.register(phoneNumber, nickname);
        });
    }

    @Test
    public void emptyNicknameTestExpectedException() throws NoSuchAlgorithmException {
        TimeBasedOneTimePasswordGenerator passwordGenerator = new TimeBasedOneTimePasswordGenerator();
        DummyUserChallenger userChallenger = new DummyUserChallenger(passwordGenerator);

        AuthServiceImpl userRegisterService = new AuthServiceImpl(userRepository, userChallenger);

        String phoneNumber = "+48123456789";
        String nickname = "";

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            userRegisterService.register(phoneNumber, nickname);
        });
    }
}