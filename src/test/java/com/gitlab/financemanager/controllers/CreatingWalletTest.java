package com.gitlab.financemanager.controllers;

import com.gitlab.financemanager.entities.User;
import com.gitlab.financemanager.services.repos.WalletRepository;
import com.gitlab.financemanager.services.wallet.WalletAlreadyExistsException;
import com.gitlab.financemanager.services.wallet.WalletServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import java.util.Currency;
import java.util.Locale;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CreatingWalletTest {

    private User user;
    private User secondUser;

    @Autowired
    private WalletRepository walletRepository;

    @PersistenceContext
    private EntityManager manager;

    @Before
    public void setUp() {
        user = User.builder()
                .phoneNumber("+48111111111")
                .nickName("nickname")
                .build();

        manager.persist(user);

        secondUser = User.builder()
                .phoneNumber("+48123456789")
                .nickName("nickname")
                .build();

        manager.persist(secondUser);
    }

    @After
    public void tearDown() {
        manager.clear();
    }

    @Test
    public void createWalletTest() throws WalletAlreadyExistsException {
        WalletServiceImpl walletService = new WalletServiceImpl(walletRepository);
        String walletName = "My Wallet";
        Currency currency = Currency.getInstance(Locale.UK);

        walletService.addNewWallet(user, walletName, currency);
        assert walletRepository.findByName(walletName).isPresent();
    }

    @Test
    public void createTwoWalletsWithSameUserTest() throws WalletAlreadyExistsException {
        WalletServiceImpl walletService = new WalletServiceImpl(walletRepository);
        String walletName1 = "My Wallet 1";
        String walletName2 = "My Wallet 2";
        Locale locale = new Locale("pl", "PL");
        Currency currency = Currency.getInstance(locale);

        walletService.addNewWallet(user, walletName1, currency);
        walletService.addNewWallet(user, walletName2, currency);

        assert walletRepository.findByName(walletName1).isPresent();
        assert walletRepository.findByName(walletName2).isPresent();
    }

    @Test
    public void createWalletWithEmptyNameTestExpectedException(){
        WalletServiceImpl walletService = new WalletServiceImpl(walletRepository);
        String walletName = "";
        Locale locale = new Locale("pl", "PL");
        Currency currency = Currency.getInstance(locale);

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            walletService.addNewWallet(user, walletName, currency);
        });
    }

    @Test
    public void createTwoWalletsWithSameNicknameTestExpectedException() throws WalletAlreadyExistsException {
        WalletServiceImpl walletService = new WalletServiceImpl(walletRepository);
        String walletName = "wallet abc";
        Locale locale = new Locale("pl", "PL");
        Currency currency = Currency.getInstance(locale);

        walletService.addNewWallet(user, walletName, currency);

        Assertions.assertThrows(WalletAlreadyExistsException.class, () -> {
            walletService.addNewWallet(user, walletName, currency);
        });
    }

    @Test
    public void createWrongCurrencyTestExpectedException(){
        Locale locale = new Locale("a", "A");

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Currency.getInstance(locale);
        });
    }

    @Test
    public void createTwoWalletsWithSameNameAndDifferentUsers() throws WalletAlreadyExistsException {
        WalletServiceImpl walletService = new WalletServiceImpl(walletRepository);
        String oneWalletName = "one name";
        Locale locale = new Locale("pl", "PL");
        Currency currency = Currency.getInstance(locale);

        walletService.addNewWallet(user, oneWalletName, currency);
        walletService.addNewWallet(secondUser, oneWalletName, currency);
    }
}
