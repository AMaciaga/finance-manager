package com.gitlab.financemanager;

import com.gitlab.financemanager.controllers.ExpenseController;
import com.gitlab.financemanager.controllers.WalletController;
import com.gitlab.financemanager.services.auth.AuthService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinanceManagerApplicationTests {

    @Autowired
    private ExpenseController expenseController;

    @Autowired
    private AuthService authService;

    @Autowired
    private WalletController walletController;

    @Test
    public void contextLoads() {
        assertThat(expenseController).isNotNull();
        assertThat(authService).isNotNull();
        assertThat(walletController).isNotNull();
    }

}
